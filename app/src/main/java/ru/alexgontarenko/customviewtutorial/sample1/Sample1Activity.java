package ru.alexgontarenko.customviewtutorial.sample1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ru.alexgontarenko.customviewtutorial.R;

/**
 * @author AlexGontarenko.
 */

public class Sample1Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample1);
    }
}
