package ru.alexgontarenko.customviewtutorial.sample7;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

import ru.alexgontarenko.customviewtutorial.R;

/**
 * @author AlexGontarenko
 */

public class AutoSizeTextView extends View {

    private TextPaint mTextPaint;
    private Rect mBounds;
    private String mText;

    public AutoSizeTextView(Context context) {
        this(context, null);
    }

    public AutoSizeTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AutoSizeTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mTextPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.density = getResources().getDisplayMetrics().density;
        mTextPaint.setFakeBoldText(false);
        mTextPaint.setTextSkewX(0);
        mTextPaint.setTextSize(10);
        mTextPaint.setColor(Color.RED);
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.AutoSizeTextView, defStyleAttr, defStyleAttr);

        if (a.hasValue(R.styleable.AutoSizeTextView_textValue)) {
            mText = a.getString(R.styleable.AutoSizeTextView_textValue);
        } else {
            mText = "";
        }
        mBounds = new Rect();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int width = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int contentWidth = width - getPaddingLeft() - getPaddingRight();
        final int height;
        if (TextUtils.isEmpty(mText)) {
            height = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        } else {
            mTextPaint.setTextSize(10);
            mTextPaint.getTextBounds(mText, 0, mText.length(), mBounds);
            float widthText = mBounds.width(),
                    heightText = mBounds.height();
            int contentHeight = (int) (heightText * contentWidth / widthText);
            height = getDefaultSize(contentHeight + getPaddingTop() + getPaddingBottom(), heightMeasureSpec);
        }
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!TextUtils.isEmpty(mText)) {
            final int contentWidth = getMeasuredWidth() - getPaddingLeft() - getPaddingRight(),
                    contentHeight = getMeasuredHeight() - getPaddingTop() - getPaddingBottom();
            final int centerX = (int) (getPaddingLeft() + contentWidth * 0.5),
                    centerY = (int) (getPaddingTop() + contentHeight * 0.5);
            mTextPaint.setTextSize(10);
            mTextPaint.getTextBounds(mText, 0, mText.length(), mBounds);
            float widthText = mBounds.width(),
                    heightText = mBounds.height();
            int textSize = Math.min((int) (10 * contentWidth / widthText), (int) (10 * contentHeight / heightText));
            mTextPaint.setTextSize(textSize);
            mTextPaint.getTextBounds(mText, 0, mText.length(), mBounds);
            float widthText_2 = 0.5f * mBounds.width(),
                    heightText_2 = 0.5f * mBounds.height();
            float yt = centerY + heightText_2;
            float xt = centerX - widthText_2;
            canvas.drawText(mText, xt, yt, mTextPaint);
        }

    }
}