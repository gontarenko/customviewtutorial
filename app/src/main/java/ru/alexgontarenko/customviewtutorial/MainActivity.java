package ru.alexgontarenko.customviewtutorial;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.alexgontarenko.customviewtutorial.sample1.Sample1Activity;
import ru.alexgontarenko.customviewtutorial.sample2.Sample2Activity;
import ru.alexgontarenko.customviewtutorial.sample3.Sample3Activity;
import ru.alexgontarenko.customviewtutorial.sample4.Sample4Activity;
import ru.alexgontarenko.customviewtutorial.sample5.Sample5Activity;
import ru.alexgontarenko.customviewtutorial.sample6.Sample6Activity;
import ru.alexgontarenko.customviewtutorial.sample7.Sample7Activity;
import ru.alexgontarenko.customviewtutorial.sample8.Sample8Activity;

public class MainActivity extends AppCompatActivity {

    private ItemClickListener mItemClickListener = new ItemClickListener();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setAdapter(new Adapter());
    }

    private final class Adapter extends RecyclerView.Adapter<ViewHolder> {

        private List<Class<?>> mItems = new ArrayList<>();

        Adapter() {
            mItems.add(Sample1Activity.class);
            mItems.add(Sample2Activity.class);
            mItems.add(Sample3Activity.class);
            mItems.add(Sample4Activity.class);
            mItems.add(Sample5Activity.class);
            mItems.add(Sample6Activity.class);
            mItems.add(Sample7Activity.class);
            mItems.add(Sample8Activity.class);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View itemView = inflater.inflate(R.layout.item_list, parent, false);
            ViewHolder holder = new ViewHolder(itemView);
            return holder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.setClass(mItems.get(position));
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private final TextView mLabel;
        private Class<?> mClazz;

        ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mLabel = (TextView) itemView.findViewById(R.id.label);
        }

        public void setClass(Class<?> clazz) {
            mClazz = clazz;
            mLabel.setText(mClazz.getSimpleName());
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            if (itemView == view && mItemClickListener != null && position != RecyclerView.NO_POSITION) {
                mItemClickListener.onItemClick(mClazz);
            }
        }
    }

    private class ItemClickListener {

        void onItemClick(Class<?> clazz) {
            startActivity(new Intent(MainActivity.this, clazz));
        }
    }
}
