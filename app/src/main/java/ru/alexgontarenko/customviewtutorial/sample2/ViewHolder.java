package ru.alexgontarenko.customviewtutorial.sample2;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import ru.alexgontarenko.customviewtutorial.R;

/**
 * @author AlexGontarenko
 */

public class ViewHolder {

    private final ImageView mImageView;
    private final TextView mTextView;

    public ViewHolder(View view) {
        mImageView = (ImageView) view.findViewById(R.id.imageView);
        mTextView = (TextView) view.findViewById(R.id.textView);
    }

    public void setData(Drawable image, String label) {
        mImageView.setImageDrawable(image);
        mTextView.setText(label);
    }
}
