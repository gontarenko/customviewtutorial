package ru.alexgontarenko.customviewtutorial.sample2;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import ru.alexgontarenko.customviewtutorial.R;

/**
 * @author AlexGontarenko
 */

public class Sample2Activity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample2);
        ViewHolder holder = new ViewHolder(findViewById(R.id.root_layout));
        holder.setData(ContextCompat.getDrawable(this, R.mipmap.ic_launcher_round), getString(R.string.other_name));
    }
}
