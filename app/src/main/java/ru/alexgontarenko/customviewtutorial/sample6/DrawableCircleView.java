package ru.alexgontarenko.customviewtutorial.sample6;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

import ru.alexgontarenko.customviewtutorial.R;

/**
 * @author AlexGontarenko
 */

public class DrawableCircleView extends View {

    private static final int MIN_SIZE = 50;
    private static final int DEFAULT_COLOR = Color.GREEN;

    private final int mMinSize;
    private int mColor;


    public DrawableCircleView(Context context) {
        this(context, null);
    }

    public DrawableCircleView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, R.style.Widget_Tutorial_CircleView);
    }

    public DrawableCircleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();

        mMinSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, MIN_SIZE, displayMetrics);
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.CircleView, defStyleAttr, defStyleAttr);

        if (a.hasValue(R.styleable.CircleView_circleColor)) {
            setColor(a.getColor(R.styleable.CircleView_circleColor, DEFAULT_COLOR));
        } else {
            setColor(DEFAULT_COLOR);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(getSize(getSuggestedMinimumWidth(), widthMeasureSpec),
                getSize(getSuggestedMinimumHeight(), heightMeasureSpec));
    }

    private static int getSize(int size, int measureSpec) {
        int result = size;
        int specMode = View.MeasureSpec.getMode(measureSpec);
        int specSize = View.MeasureSpec.getSize(measureSpec);

        switch (specMode) {
            case View.MeasureSpec.UNSPECIFIED:
            case View.MeasureSpec.EXACTLY:
                result = size;
                break;
            case View.MeasureSpec.AT_MOST:
                result = Math.min(specSize, result);
                break;
        }
        return result;
    }

    @Override
    protected int getSuggestedMinimumWidth() {
        return getPaddingLeft() + getPaddingRight() + mMinSize;
    }

    @Override
    protected int getSuggestedMinimumHeight() {
        return getPaddingTop() + getPaddingBottom() + mMinSize;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        final float widthContent_2 = (getMeasuredWidth() - getPaddingLeft() - getPaddingRight()) * 0.5f,
                heihtContent_2 = (getMeasuredHeight() - getPaddingTop() - getPaddingLeft()) * 0.5f;
        final float radius = Math.min(widthContent_2, heihtContent_2),
                centerX = getPaddingLeft() + widthContent_2,
                centerY = getPaddingTop() + heihtContent_2;

        Paint paint = new Paint();
        paint.setColor(mColor);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        canvas.drawCircle(centerX, centerY, radius, paint);
    }

    public int getColor() {
        return mColor;
    }

    public void setColor(int color) {
        mColor = color;
        invalidate();
    }
}
