package ru.alexgontarenko.customviewtutorial.sample4;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.alexgontarenko.customviewtutorial.R;

/**
 * @author AlexGontarenko
 */

public class CustomView extends LinearLayout {

    private final ImageView mImageView;
    private final TextView mTextView;

    public CustomView(Context context) {
        this(context, null);
    }

    public CustomView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, R.style.Widget_Tutorial_CustomView);
    }

    public CustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        super.setOrientation(VERTICAL);
        LayoutInflater.from(getContext()).inflate(R.layout.view_layout, this);
        mImageView = (ImageView) findViewById(R.id.imageView);
        mTextView = (TextView) findViewById(R.id.textView);

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.CustomView, defStyleAttr, defStyleAttr);

        int id;
        if (a.hasValue(R.styleable.CustomView_imageSrc)) {
            setDrawable(a.getDrawable(R.styleable.CustomView_imageSrc));
        } else {
            setDrawable(null);
        }

        if (a.hasValue(R.styleable.CustomView_styleTextAppearance)) {
            id = a.getResourceId(R.styleable.CustomView_styleTextAppearance, -1);
            mTextView.setTextAppearance(mTextView.getContext(), id);
        }

        if (a.hasValue(R.styleable.CustomView_textLabel)) {
            setLabel(a.getText(R.styleable.CustomView_textLabel));
        } else {
            setDrawable(null);
        }
        a.recycle();
    }

    @Override
    public void setOrientation(int orientation) {

    }

    public void setDrawable(Drawable drawable) {
        mImageView.setImageDrawable(drawable);
        mImageView.setVisibility(drawable != null ? VISIBLE : GONE);
    }

    public void setLabel(CharSequence label) {
        mTextView.setText(label);
        mTextView.setVisibility(TextUtils.isEmpty(label) ? GONE : VISIBLE);
    }

    public Drawable getDrawable() {
        return mImageView.getDrawable();
    }

    public CharSequence getLabel() {
        return mTextView.getText();
    }

}
