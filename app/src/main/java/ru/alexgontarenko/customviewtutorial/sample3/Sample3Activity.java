package ru.alexgontarenko.customviewtutorial.sample3;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import ru.alexgontarenko.customviewtutorial.R;

/**
 * @author AlexGontarenko
 */

public class Sample3Activity extends AppCompatActivity
        implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample3);
        findViewById(R.id.example1).setOnClickListener(this);
        findViewById(R.id.example2).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.example1:
                startActivity(new Intent(this, Example1Activity.class));
                break;
            case R.id.example2:
                startActivity(new Intent(this, Example2Activity.class));
                break;
        }
    }
}
