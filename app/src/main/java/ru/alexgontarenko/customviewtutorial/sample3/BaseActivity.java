package ru.alexgontarenko.customviewtutorial.sample3;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ru.alexgontarenko.customviewtutorial.R;

/**
 * @author AlexGontarenko
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample3_base);
    }

}
