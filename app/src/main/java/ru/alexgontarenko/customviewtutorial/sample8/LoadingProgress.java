package ru.alexgontarenko.customviewtutorial.sample8;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.NonNull;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.LinearInterpolator;

/**
 * @author AlexGontarenko
 */

public class LoadingProgress extends View {

    private TextPaint mLabel;
    private int mLabelPadding;
    private final float mMaxTextWidth;
    private final float mRadius;
    private final float mStrokeWidth;
    private final static int sMax = 100;
    private int mProgress;
    private float mStartProgressAngel = -90;
    private static final String FORMAT = "%d%%";
    /**
     * Флаг дял отслеживания когда можно запускать анимацию
     */
    private boolean mAggregatedIsVisible;

    /**
     * Анимация загрузки данных
     */
    private ValueAnimator mLoadingAnimator;


    public LoadingProgress(Context context) {
        this(context, null);
    }

    public LoadingProgress(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LoadingProgress(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mLabel = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        mLabel.density = getResources().getDisplayMetrics().density;


        mLabel.setFakeBoldText(false);
        mLabel.setTextSkewX(0);
        mLabel.setTextSize(15);
        mLabel.setColor(Color.RED);
        Resources resources = getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();

        mLabelPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, displayMetrics);

        mMaxTextWidth = mLabel.measureText(String.format(FORMAT, 100));
        mStrokeWidth = 0.5f * mLabelPadding;
        mRadius = mMaxTextWidth + 2 * mLabelPadding;
    }

    public int getProgress() {
        return mProgress;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int size = (int) mMaxTextWidth + 4 * mLabelPadding;
        final int width = getPaddingLeft() + getPaddingRight() + size,
                height = getPaddingTop() + getPaddingBottom() + size;

        final int measuredWidth = resolveSizeAndState(width, widthMeasureSpec, 0),
                measuredHeight = resolveSizeAndState(height, heightMeasureSpec, 0);
        setMeasuredDimension(measuredWidth, measuredHeight);
    }

    public synchronized void setProgress(int progress) {
        progress = constrain(progress, 0, sMax);

        if (progress != mProgress) {
            mProgress = progress;
            if(progress<sMax) {
                startAnimation();
            } else {
                stopAnimation();
            }
            invalidate();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        final int centerX = (int) ((getMeasuredWidth() + getPaddingLeft() - getPaddingRight()) * 0.5),
                centerY = (int) ((getMeasuredHeight() + getPaddingTop() - getPaddingBottom()) * 0.5);
        final float offset = 0.5f * mStrokeWidth;
        RectF rect = new RectF(getPaddingLeft() + offset
                , getPaddingTop() + offset
                , getMeasuredWidth() - getPaddingRight() - offset
                , getMeasuredHeight() - getPaddingBottom() - offset);
        int progress = 100 * getProgress() / sMax;
        drawBachground(canvas, rect);
        drawProgress(canvas, rect, progress);
        drawLabel(canvas, centerX, centerY, progress);

    }

    protected void drawBachground(@NonNull Canvas canvas, RectF rectF) {
        Paint paint = new Paint();
        paint.setStrokeWidth(mStrokeWidth);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.GREEN);
        canvas.drawArc(rectF, 0f, 360f, false, paint);
    }

    protected void drawProgress(@NonNull Canvas canvas, RectF rectF, int progress) {
        Paint paint = new Paint();

        paint.setStrokeWidth(mStrokeWidth);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.RED);
        canvas.drawArc(rectF, mStartProgressAngel, arc(progress), false, paint);
    }

    protected void drawLabel(@NonNull Canvas canvas,
                             int centerX, int centerY, int progress) {
        final TextPaint textPaint = new TextPaint(mLabel);

        String label = String.format(FORMAT, progress);
        Rect bounds = new Rect();
        textPaint.getTextBounds(label, 0, label.length(), bounds);
        float widthText_2 = 0.5f * bounds.width(),
                heightText_2 = 0.5f * bounds.height();
        float yt = centerY + heightText_2;
        float xt = centerX - widthText_2;
        canvas.drawText(label, xt, yt, textPaint);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        startAnimation();
    }

    @Override
    protected void onDetachedFromWindow() {
        stopAnimation();
        super.onDetachedFromWindow();

    }

    @Override
    public void onVisibilityAggregated(boolean isVisible) {
        super.onVisibilityAggregated(isVisible);
        if (isVisible != mAggregatedIsVisible) {
            mAggregatedIsVisible = isVisible;
            if (isVisible) {
                startAnimation();
            } else {
                stopAnimation();
            }
        }
    }

    private void startAnimation() {
        if (mLoadingAnimator != null) {
            return;
        }
        mStartProgressAngel = -90;
        mLoadingAnimator = ValueAnimator.ofFloat(-90, 270);
        mLoadingAnimator.setDuration(600);
        mLoadingAnimator.setRepeatCount(ValueAnimator.INFINITE);
        mLoadingAnimator.setInterpolator(new LinearInterpolator());
        mLoadingAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mStartProgressAngel = ((Float) (animation.getAnimatedValue())).floatValue();
                postInvalidate();
            }
        });
        mLoadingAnimator.start();
    }

    private void stopAnimation() {
        if (mLoadingAnimator != null) {
            mLoadingAnimator.removeAllUpdateListeners();
            mLoadingAnimator.end();
            mLoadingAnimator.cancel();
            mLoadingAnimator = null;
        }
    }

    private static float arc(int progress) {
        return 60 + 3 * progress;
    }

    private static int constrain(int amount, int low, int high) {
        return amount < low ? low : (amount > high ? high : amount);
    }
}
