package ru.alexgontarenko.customviewtutorial.sample8;

import android.animation.ValueAnimator;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.LinearInterpolator;

import ru.alexgontarenko.customviewtutorial.R;

/**
 * @author AlexGontarenko
 */

public class Sample8Activity extends AppCompatActivity implements View.OnClickListener{

    LoadingProgress mProgress;

    private ValueAnimator mLoadingAnimator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample8);
        mProgress = (LoadingProgress) findViewById(R.id.progress);
        findViewById(R.id.start).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(mLoadingAnimator!=null) {
            mLoadingAnimator.removeAllUpdateListeners();
            mLoadingAnimator.end();
            mLoadingAnimator.cancel();
            mLoadingAnimator = null;
        }
        mLoadingAnimator = ValueAnimator.ofInt(0, 100);
        mLoadingAnimator.setDuration(5000);
        mLoadingAnimator.setRepeatCount(0);
        mLoadingAnimator.setInterpolator(new LinearInterpolator());
        mLoadingAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mProgress.setProgress(((Integer) (animation.getAnimatedValue())).intValue());
            }
        });
        mLoadingAnimator.start();
    }
}
